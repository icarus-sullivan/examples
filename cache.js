export const cache = async (fn, ttyl) => {
  let last = Date.now();
  let memo;

  if (Date.now() - last < ttyl) {
    return memo;
  }

  return async (...args) => {
    memo = await fn(...args);
    last = Date.now();
    return memo;
  };
};