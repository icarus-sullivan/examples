

const settle = (ps) =>
  Promise.all(
    ps.map((p) =>
      Promise.resolve(p)
        .then((res) => ({
          success: true,
          value: res,
        }))
        .catch((err) => ({
          success: false,
          reason: err,
        })),
    ),
  );
  
 modul.exports = { settle };