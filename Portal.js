import ReactDOM from 'react-dom';

const PortalElement = ({ id, children }) => ReactDOM.createPortal(
  children,
  id ? document.getElementById(id) : document.body,
);

export default PortalElement;