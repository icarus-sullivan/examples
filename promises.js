export const sleep = millis =>
  new Promise(resolve => setTimeout(resolve, millis));

export const race = (...promises) =>
  new Promise(resolve => promises.map(p => Promise.resolve(p).then(resolve)));

export const sequence = promises =>
  promises.reduce((a, fn) => {
    if (a && a.then && typeof a.then === 'function') {
      return a.then(fn);
    }
    return fn(a);
  }, Promise.resolve());

export const settle = promises =>
  Promise.resolve(promises).then(things => {
    if (!Array.isArray(things)) {
      throw new Error('Settle expects an array of promises');
    }

    return Promise.all(
      things.map(p => {
        return Promise.resolve(p).then(
          res => ({
            success: true,
            value: res,
          }),
          err => ({
            success: false,
            reason: err,
          }),
        );
      }),
    );
  });