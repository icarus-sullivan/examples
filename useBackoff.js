import { useEffect, useRef } from 'react';
export const LINEAR_STRATEGY = ({ delay, i }) => (i + 1) * 30000;
export const EXPONENTIAL_STRATEGY = ({ delay, i }) =>
  Math.min(Number.MAX_SAFE_INTEGER, delay ** 2);
const useBackoff = (fn, strategy = LINEAR_STRATEGY, whenChanged = []) => {
  const ref = useRef({
    i: 0,
    delay: 0,
  });
  useEffect(() => {
    let handle;
    const interval = () => {
      fn();
      const computedDelay = strategy(ref.current);
      // If the strategy returns a negative number stop execution
      if (computedDelay < 0) return;
      ref.current.i += 1;
      ref.current.delay = computedDelay;
      handle = setTimeout(interval, computedDelay);
    };
    handle = setTimeout(interval, ref.current.delay);
    return () => {
      clearTimeout(handle);
      // reset current in case of re-use
      ref.current = {
        i: 0,
        delay: 0,
      };
    };
  }, [whenChanged]);
};
export default useBackoff;