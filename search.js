const readline = require('readline');

const build_node = (obj, str) => {
  const chars = str.split('');
  const len = chars.length;
  chars.reduce((a, s, i) => {
    if (!a[s]) {
      a[s] = {};
    }

    if (i === len - 1) {
      a[s].matches = (a[s].matches || []);
      a[s].matches.push(str);
    }
    return a[s];
  }, obj);
}


const emails = [
  'botippenne+1428@gmail.com',
  'bot@domain.org',
  'emmukadduf+1302@mail.com',
  'alleqabeku+7001@yahoo.com',
  'alleqabeku+7003@yahoo.com',
  'all+7001@yahoo.com',
  'figessebus+2466@comcast.net',
  'fignewtwon+2466@comcast.net',
  'orriruffudde+6394@gmail.com',
  'oof@troller.io'
]

// constructed tree from emails (non-balanced)
const tree = emails.reduce((root, email) => (build_node(root, email), root), {});

// recursively search for matches in nodes
const get_matches = (node) => {
  if (node.matches) {
    return node.matches;
  }

  // flatten array of arrays
  return [].concat.apply(
     [],
     Object.values(node).map(get_matches)
  );
}

// look for data in our tree
const query = (p) => {
  let found = tree;
  for (const c of p) {
    if (found[c]) {
      found = found[c];
    } else {
      found = {};
    }
  }

  return get_matches(found);
}

// output suggestions and input dynamically
const display = (terms) => {
  const results = query(terms.join(''));
  console.log('Suggestions:', JSON.stringify(results, null, 2));
  console.log('>', terms.join(''));
}


const rl = readline.createInterface({
  input: process.stdin,
  // do not write to output, we will be doing that with console
  // output: process.stdout,
  terminal: true,
  prompt: '> '
});

console.log('search:\n>:');
let term = [];
// Had to use weird local state to listen to key presses dynamically
// readline does use 'completer' but only works when enter is pressed
rl.input.on('keypress',function( char, key ) {
  if( key == undefined || key.name === 'tab') {
    // do nothing
  } else if (key.name === 'escape') {
    process.exit();
  } else if (key.name === 'backspace') {
    term.pop();
    display(term);
  } else if (key.name === 'return' ) {
    console.log('You entered: ', term.join(''));
    process.exit();
  } else {
    term.push(key.name || char);
    display(term);
  }
}); 